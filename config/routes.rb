Rails.application.routes.draw do

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
 
  root 'home#home'
  get '/settings', to: 'admin#settings'
  get '/create', to: 'patrons#new'
  post '/create', to: 'patrons#create'
  get '/createOrder', to: 'orders#new'
  post '/createOrder', to: 'orders#create'
  get '/edit', to: 'orders#edit'
  get '/viewOrders', to: 'orders#viewOrders'
  
  resources :orders, :patrons, :posters, :threedprints
  
end

