class Threedprint < ApplicationRecord
  belongs_to :order
  
  default_scope -> { order(created_at: :desc) }
  validates :file_name, presence: true, length: {maximum: 30}
  validates :quantity, presence: true, length: {maximum: 30}
  validates :price, presence: true, length: {maximum: 30}
  validates :price_per_m, presence: true, length: {maximum: 30}
  validates :color, presence: true, length: {maximum: 30}
  validates :scaling, presence: true, length: {maximum: 30}
  validates :platform_adhesion, presence: true, length: {maximum: 30}
  validates :supports, presence: true, length: {maximum: 30}
  #NO VALIDATION FOR COMMENTS
end
