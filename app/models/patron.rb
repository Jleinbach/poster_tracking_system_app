class Patron < ApplicationRecord
  has_many :orders
  
  before_save {self.email = email.downcase}
  before_save {self.first_name = first_name.slice(0,1).capitalize + first_name.slice(1..-1).downcase}
  before_save {self.last_name = last_name.slice(0,1).capitalize + last_name.slice(1..-1).downcase}
  
  validates :first_name, presence: true, length: {maximum: 25}
  validates :last_name, presence: true, length: {maximum: 30}
  
  ##TODO: Add so Phone Regex Takes "/" inbetween characters
    #Or have form automatcially create 3 boxes with - between
  VALID_PHONE_REGEX = /\A\(?\d{3}\)?[- ]?\d{3}[- ]?\d{4}\z/
  validates :phone, presence: true, length: {maximum: 20},
                    format: { with: VALID_PHONE_REGEX}
  
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false}
  ##has_secure_password
  
  ##validates :password, length: { minimum: 6 }
end
