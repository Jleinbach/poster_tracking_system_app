class Order < ApplicationRecord
  #belongs_to :patrons
  has_many :posters, dependent: :destroy
  has_many :threedprints, dependent: :destroy
  accepts_nested_attributes_for :posters, :allow_destroy => true
  accepts_nested_attributes_for :threedprints, :allow_destroy => true
  #accepts_nested_attributes_for :patrons 
  
  default_scope -> { order(created_at: :desc) }
  validates :patron_id, presence: true, length: {maximum: 30}
  validates :print_location, presence: true, length: {maximum: 30}
  validates :payment, presence: true, length: {maximum: 30}
  validates :status, presence: true, length: {maximum: 30}
  validates :staff_print, presence: true, length: {maximum: 30}
  validates :takenby, presence: true, length: {maximum: 30}
end
