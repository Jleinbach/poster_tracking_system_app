class Poster < ApplicationRecord
  belongs_to :order
  
  default_scope -> { order(created_at: :desc) }
  validates :file_name, presence: true, length: {maximum: 30}
  validates :patron_id, presence: true, length: {maximum: 20}
  validates :quantity, presence: true, length: {maximum: 30}
  validates :price, presence: true, length: {maximum: 30}
  validates :width, presence: true, length: {maximum: 30}
  validates :height, presence: true, length: {maximum: 30}
  validates :price_per_sqft, presence: true, length: {maximum: 30}
  validates :paper_type, presence: true, length: {maximum: 30}
  #NO VALIDATION FOR COMMENTS
end
