class OrdersDatatable < ApplicationDatatable
  #delegate :edit_user_path, to: :@view

  private

  def data
    orders.map do |order|
      [].tap do |column|
        column << order.id
        column << order.payment
        column << order.status
        column << order.takenby

        links = []
        links << link_to('Edit', edit_path(order))
        column << links.join(' | ')
      end
    end
  end

  def count
    Order.count
  end

  def total_entries
    orders.total_count
    # will_paginate
  end

  def orders
    @orders ||= fetch_orders
  end

  def fetch_orders
    search_string = []
    columns.each do |term|
      search_string << "#{term} like :search"
    end

    # will_paginate
    # users = User.page(page).per_page(per_page)
    orders = Order.order("#{sort_column} #{sort_direction}")
    orders = orders.page(page).per(per_page)
    orders = orders.where(search_string.join(' or '), search: "%#{params[:search][:value]}%")
  end

  def columns
    %w(id payment status takenby)
  end
end