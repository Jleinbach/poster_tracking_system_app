class PatronsController < ApplicationController
  def new
    @patron = Patron.new
  end

  #Order forms that don't have all required fields
  def create
    @patron = Patron.create(patron_params)
    
    if @patron.save
      #handle save
      flash[:success] = "Patron Created Sucessfully!"
      redirect_to createOrder_path
    else
      render 'new'
    end
    
  end
  
  private
  
    def patron_params
      params.require(:patron).permit(:id, :first_name, :last_name, :phone, :email)
    end
    
  
end
