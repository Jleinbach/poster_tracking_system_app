class OrdersController < ApplicationController
  #Original call for order forms
  def new
    @order = Order.new
    1.times {@order.posters.build}
  end
  
  def show
  end

  def index
    @orders = Order.all
  end
  
  #Order forms that don't have all required fields
  def create
    @order = Order.create(order_params)
    
    if @order.save
      #handle save
      flash[:success] = "Order Created Sucessfully!"
      redirect_to viewOrders_path
    else
      render 'new'
    end
    
  end
  
  def edit
    @order = Order.find(params[:id])
  end 
  
  def update
    @order = Order.find(params[:id])
    if @order.update_attributes(order_params)
      # Handle a successful update.
      flash[:success] = "Order Updated Sucessfully!"
      redirect_to viewOrders_path
    else
      render 'edit'
    end
  end
    
  def destroy
    Order.find(params[:id]).destroy
    flash[:success] = "Order Deleted Sucessfully!"
    redirect_to viewOrders_path
  end

  def viewOrders
    @orders = Order.all
  end
  
  
  
  private
  
    def order_params
      params.require(:order).permit(:patron_id, :print_location, :payment, :status, :staff_print, :takenby,
      posters_attributes:[:file_name, :patron_id, :quantity, :price, :width, :height, :comments, :price_per_sqft, :paper_type, :id])
    end
    
end
