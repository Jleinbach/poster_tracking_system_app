class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.integer :patron_id
      t.string :print_location
      t.string :payment
      t.string :status
      t.boolean :staff_print
      t.string :takenby

      t.timestamps
    end
  end
end
