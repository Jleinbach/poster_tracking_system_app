class AddOrderIdToPosters < ActiveRecord::Migration[5.1]
  def change
    add_column :posters, :order_id, :integer
    add_index :posters, :order_id
  end
end
