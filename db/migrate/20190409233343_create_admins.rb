class CreateAdmins < ActiveRecord::Migration[5.1]
  def change
    create_table :admins do |t|
      t.decimal :price_per_sqft
      t.decimal :price_per_m

      t.timestamps
    end
  end
end
