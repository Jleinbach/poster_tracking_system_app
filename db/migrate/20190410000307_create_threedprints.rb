class CreateThreedprints < ActiveRecord::Migration[5.1]
  def change
    create_table :threedprints do |t|
      t.integer :order_id
      t.string :file_name
      t.integer :quantity
      t.decimal :length
      t.decimal :price_per_m
      t.string :color
      t.text :comments
      t.integer :scaling
      t.string :supports
      t.string :platform_adhension
      t.decimal :price

      t.timestamps
    end
  end
end
