class CreatePosters < ActiveRecord::Migration[5.1]
  def change
    create_table :posters do |t|
      t.string :file_name
      t.integer :patron_id
      t.integer :quantity
      t.decimal :price
      t.decimal :width
      t.decimal :height
      t.text :comments
      t.decimal :price_per_sqft
      t.string :paper_type

      t.timestamps
    end
  end
end
