require 'test_helper'

class AdminControllerControllerTest < ActionDispatch::IntegrationTest
  test "should get settings" do
    get admin_controller_settings_url
    assert_response :success
  end

end
